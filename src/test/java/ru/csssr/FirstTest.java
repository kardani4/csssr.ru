package ru.csssr;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import java.util.ArrayList;

public class FirstTest {
    public WebDriver driver;

    @BeforeTest
    public void before () {
        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("http://csssr.github.io/qa-engineer/");
    }
    @Test
    public void test1CheckUrl() {


        String urlActual = driver.getCurrentUrl();
        String urlExpected = "http://csssr.github.io/qa-engineer/";

        Assert.assertEquals(urlActual,urlExpected,"page url is not equals!");
    }

    @Test
    public void test2CheckLink () throws Exception{
        driver.findElement(By.xpath("/html/body/div[1]/section[1]/section/div[2]/a")).click();



        String oldTab = driver.getWindowHandle();
        driver.findElement(By.linkText("Софт для быстрого создания скриншотов")).click();
        ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
        newTab.remove(oldTab);
        // change focus to new tab
        driver.switchTo().window(newTab.get(0));

        String softUrlAct = driver.getCurrentUrl();
        String softUrlExp = "http://monosnap.com/";

        Assert.assertEquals(softUrlAct, softUrlExp, "link address is not equals!");
    }

    @AfterTest
    public void after () {
        driver.quit();
    }
}